// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:

const gradeStudent = (grades) => {
    let mark = ''
    let sum = 0
    grades.forEach(grade => {
        sum += grade
    })
    const average = sum / grades.length

        if (average <= 74) {
            mark = 'Failed'
        }else if (average <= 80) {
            mark = 'Beginner'
        }else if (average <= 85) {
            mark = 'Developing'
        } else if (average <= 85) {
            mark = 'Above Average'
        } else {
            mark = 'Advanced'
        }

    console.log(`Your quarterly average is ${average}. You have received a ${mark} mark.`)
}

gradeStudent([84, 93, 95, 74])

/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here:

for (let i = 1; i <= 300; i++) {
    console.log(`${i} - ${i % 2 === 0 ? 'even': 'odd'}`)
}


/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/

const heroName = prompt('Hero name :')
const origin = prompt('Hero origin :')
const description = prompt('Description :')
const skill1 = prompt('Skill 1: ')
const skill2 = prompt('Skill 2: ')
const skill3 = prompt('Skill 3:')

const hero = {
    heroName: heroName,
    origin: origin,
    description: description,
    skills: {
        skill1: skill1,
        skill2: skill2,
        skill3: skill3
    }
}

console.log(hero)